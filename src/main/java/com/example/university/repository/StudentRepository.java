package com.example.university.repository;

import com.example.university.entity.Group;
import com.example.university.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student,Long> {
    Boolean existsByName(String name);
    Optional<Student> findByDeletedFalseAndId(Long id);
    List<Student> findAllByDeletedFalse();
}
