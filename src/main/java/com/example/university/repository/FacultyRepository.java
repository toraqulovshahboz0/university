package com.example.university.repository;
import com.example.university.entity.Faculty;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import java.util.Optional;

public interface FacultyRepository extends JpaRepository<Faculty,Long> {
    Boolean existsByName(String name);
    Optional<Faculty> findByDeletedFalseAndId(Long id);
    List<Faculty> findAllByDeletedFalse();
}
