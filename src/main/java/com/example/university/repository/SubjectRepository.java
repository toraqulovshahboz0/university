package com.example.university.repository;

import com.example.university.entity.Group;
import com.example.university.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SubjectRepository extends JpaRepository<Subject,Long> {
    Boolean existsByName(String name);
    Optional<Subject> findByDeletedFalseAndId(Long id);
    List<Subject> findAllByDeletedFalse();
    Optional<Subject> findAllBy(Long id);


}
