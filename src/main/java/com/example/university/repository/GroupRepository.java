package com.example.university.repository;

import com.example.university.entity.Faculty;
import com.example.university.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface GroupRepository extends JpaRepository<Group, Long> {
    Boolean existsByName(String name);
    Optional<Group> findByDeletedFalseAndId(Long id);
    List<Group> findAllByDeletedFalse();
}
