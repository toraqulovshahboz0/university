package com.example.university.repository;

import com.example.university.entity.Journal;
import com.example.university.entity.Mark;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MarkRepository extends JpaRepository<Mark, Long> {

    Optional<Mark> findByDeletedFalseAndId(Long id);

    List<Mark> findAllByDeletedFalse();
}
