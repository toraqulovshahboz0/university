package com.example.university.repository;

import com.example.university.entity.University;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UniversityRepository extends JpaRepository<University,Long> {
    Boolean existsByName(String name);
    Optional<University> findByDeletedFalseAndId(Long id);
    List<University> findAllByDeletedFalse();

}
