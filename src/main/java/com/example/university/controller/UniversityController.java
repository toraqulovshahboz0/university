package com.example.university.controller;

import com.example.university.dto.UniversityDto;
import com.example.university.service.university.UniversityService;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/v1/university")
public class UniversityController {
    private final UniversityService service;

    public UniversityController(UniversityService service) {
        this.service = service;
    }

    @PostMapping
    UniversityDto add(@RequestBody UniversityDto dto) {
        return service.add(dto);
    }

    @GetMapping("/{id}")
    UniversityDto getOne(@PathVariable Long id) {
        return service.getOne(id);
    }

    @GetMapping
    List<UniversityDto> findAll() {
        return service.findAll();
    }

    @PutMapping("/{id}")
    UniversityDto update(@PathVariable Long id, @RequestBody UniversityDto dto) {
        return service.update(id, dto);
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id) {
        service.delete(id);
    }

}
