package com.example.university.controller;

import com.example.university.dto.GroupDto;
import com.example.university.dto.JournalDto;
import com.example.university.service.journal.JournalService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/journal")
public class JournalController {
    private final JournalService service;

    @PostMapping
    JournalDto add(@RequestBody JournalDto dto) {
        return service.add(dto);
    }

    @GetMapping("/{id}")
    JournalDto getOne(@PathVariable Long id) {
        return service.getOne(id);
    }

    @GetMapping
    List<JournalDto> findAll() {
        return service.findAll();
    }

    @PutMapping("/{id}")
    JournalDto update(@PathVariable Long id, @RequestBody JournalDto dto) {
        return service.update(id, dto);
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id) {
        service.delete(id);
    }

}

