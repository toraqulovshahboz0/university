package com.example.university.controller;

import com.example.university.dto.FacultyDto;
import com.example.university.service.faculty.FacultyService;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/v1/faculty")
public class FacultyController {

    private final FacultyService service;

    public FacultyController(FacultyService service) {
        this.service = service;
    }

    @PostMapping
    FacultyDto add(@RequestBody FacultyDto dto) {
        return service.add(dto);
    }

    @GetMapping("/{id}")
    FacultyDto getOne(@PathVariable Long id) {
        return service.getOne(id);
    }

    @GetMapping
    List<FacultyDto> findAll() {
        return service.findAll();
    }

    @PutMapping("/{id}")
    FacultyDto update(@PathVariable Long id, @RequestBody FacultyDto dto) {
        return service.update(id, dto);
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id) {
        service.delete(id);
    }
}
