package com.example.university.controller;

import com.example.university.dto.StudentDto;
import com.example.university.dto.UniversityDto;
import com.example.university.service.student.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/student")
public class StudentController {
    private final StudentService service;
    @PostMapping
    StudentDto add(@RequestBody StudentDto dto) {
        return service.add(dto);
    }

    @GetMapping("/{id}")
    StudentDto getOne(@PathVariable Long id) {
        return service.getOne(id);
    }

    @GetMapping
    List<StudentDto> findAll() {
        return service.findAll();
    }

    @PutMapping("/{id}")
    StudentDto update(@PathVariable Long id, @RequestBody StudentDto dto) {
        return service.update(id, dto);
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id) {
        service.delete(id);
    }

}
