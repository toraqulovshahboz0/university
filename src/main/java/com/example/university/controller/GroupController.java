package com.example.university.controller;

import com.example.university.dto.FacultyDto;
import com.example.university.dto.GroupDto;
import com.example.university.service.group.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/group")
public class GroupController {
    private final GroupService service;

    @PostMapping
    GroupDto add(@RequestBody GroupDto dto) {
        return service.add(dto);
    }

    @GetMapping("/{id}")
    GroupDto getOne(@PathVariable Long id) {
        return service.getOne(id);
    }

    @GetMapping
    List<GroupDto> findAll() {
        return service.findAll();
    }

    @PutMapping("/{id}")
    GroupDto update(@PathVariable Long id, @RequestBody GroupDto dto) {
        return service.update(id, dto);
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id) {
        service.delete(id);
    }

}
