package com.example.university.controller;

import com.example.university.dto.MarkDto;
import com.example.university.dto.StudentDto;
import com.example.university.service.mark.MarkService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/mark")
public class MarkController {
    private final MarkService service;

    @PostMapping
    MarkDto add(@RequestBody MarkDto dto) {
        return service.add(dto);
    }

    @GetMapping("/{id}")
    MarkDto getOne(@PathVariable Long id) {
        return service.getOne(id);
    }

    @GetMapping
    List<MarkDto> findAll() {
        return service.findAll();
    }

    @PutMapping("/{id}")
    MarkDto update(@PathVariable Long id, @RequestBody MarkDto dto) {
        return service.update(id, dto);
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id) {
        service.delete(id);
    }

}

