package com.example.university.controller;

import com.example.university.dto.GroupDto;
import com.example.university.dto.SubjectDto;
import com.example.university.service.subject.SubjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/subject")
public class SubjectController {
    private final SubjectService service;

    @PostMapping
    SubjectDto add(@RequestBody SubjectDto dto) {
        return service.add(dto);
    }

    @GetMapping("/{id}")
    SubjectDto getOne(@PathVariable Long id) {
        return service.getOne(id);
    }

    @GetMapping
    List<SubjectDto> findAll() {
        return service.findAll();
    }

    @PutMapping("/{id}")
    SubjectDto update(@PathVariable Long id, @RequestBody SubjectDto dto) {
        return service.update(id, dto);
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id) {
        service.delete(id);
    }

}

