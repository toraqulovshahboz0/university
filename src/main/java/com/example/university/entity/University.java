package com.example.university.entity;

import lombok.*;

import javax.persistence.*;

@Builder
@EqualsAndHashCode(callSuper = true)
@Setter
@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class University extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 128)
    private String name;
    @Column(length = 128)
    private String address;
    @Column(length = 64)
    private Integer openYear;


}
