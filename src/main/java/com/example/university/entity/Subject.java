package com.example.university.entity;

import lombok.*;

import javax.persistence.*;
@Builder
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Subject extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 128)
    private String name;
    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;

}
