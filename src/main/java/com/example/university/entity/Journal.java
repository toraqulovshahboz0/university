package com.example.university.entity;

import lombok.*;

import javax.persistence.*;
@Builder
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Journal extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 64)
    private String name;
    @OneToOne
    @JoinColumn(name = "group_id")
    private Group group;
}
