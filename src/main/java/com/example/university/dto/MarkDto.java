package com.example.university.dto;

import com.example.university.entity.Mark;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class MarkDto {
    private Long id;
    private Integer mark;
    private Long studentId;
    private Long journalId;

    public static MarkDto toDto(Mark mark) {
        return MarkDto.builder()
                .id(mark.getId())
                .mark(mark.getMark())
                .studentId(mark.getStudent().getId())
                .journalId(mark.getJournal().getId())
                .build();
    }
    public static Mark toEntity(MarkDto dto){
        return Mark.builder()
                .id(dto.getId())
                .mark(dto.getMark())
                .build();
    }

}
