package com.example.university.dto;

import com.example.university.entity.Student;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class StudentDto {
    private Long id;
    private String name;
    private Long groupId;

    public static StudentDto toDto(Student student) {
        return StudentDto.builder()
                .id(student.getId())
                .name(student.getName())
                .groupId(student.getGroup().getId())
                .build();
    }
    public static Student toEntity(StudentDto dto){
        return Student.builder()
                .id(dto.getId())
                .name(dto.getName())
                .build();
    }
}
