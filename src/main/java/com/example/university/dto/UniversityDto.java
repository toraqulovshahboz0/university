package com.example.university.dto;

import com.example.university.entity.University;
import lombok.*;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UniversityDto {
    private Long id;
    private String name;
    private String address;
    private Integer openYear;


    public static UniversityDto toDto(University university) {
        return UniversityDto.builder()
                .id(university.getId())
                .name(university.getName())
                .address(university.getAddress())
                .openYear(university.getOpenYear())
                .build();
    }


    public static University toEntity(UniversityDto dto) {
        return University.builder()
                .id(dto.getId())
                .name(dto.getName())
                .address(dto.getAddress())
                .openYear(dto.openYear)
                .build();


    }
}
