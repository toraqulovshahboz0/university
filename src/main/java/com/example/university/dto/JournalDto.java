package com.example.university.dto;

import com.example.university.entity.Journal;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class JournalDto {
    private Long id;
    private String name;
    private Long groupId;

    public static JournalDto toDto(Journal journal){
       return JournalDto.builder()
                .id(journal.getId())
                .name(journal.getName())
               .groupId(journal.getGroup().getId())
               .build();
    }
    public static Journal toEntity(JournalDto dto){
        return Journal.builder()
                .id(dto.getId())
                .name(dto.getName())
                .build();
    }
}
