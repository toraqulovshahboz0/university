package com.example.university.dto;

import com.example.university.entity.Faculty;
import com.example.university.entity.University;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class FacultyDto {
    private Long id;
    private String name;
    private Long universityId;

    public static FacultyDto toDto(Faculty faculty) {
        return FacultyDto.builder()
                .id(faculty.getId())
                .name(faculty.getName())
                .universityId(faculty.getUniversity().getId())
                .build();
    }
    public static Faculty toEntity(FacultyDto facultyDto) {
        return Faculty.builder()
                .name(facultyDto.getName())
                .id(facultyDto.getId())
                .build();
    }
}
