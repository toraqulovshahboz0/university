package com.example.university.dto;

import com.example.university.entity.Subject;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SubjectDto {
    private Long id;
    private String name;
    private Long groupId;

    public static SubjectDto toDto(Subject subject){
       return SubjectDto.builder()
                .id(subject.getId())
               .name(subject.getName())
               .groupId(subject.getGroup().getId())
               .build();
    }
    public static Subject toEntity(SubjectDto dto){
        return Subject.builder()
                .id(dto.getId())
                .name(dto.getName())
                .build();

    }
}
