package com.example.university.dto;

import com.example.university.entity.Group;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class GroupDto {
    private Long id;
    private String name;
    private Integer year;
    private Long facultyId;

    public static GroupDto toDto(Group group){
        return GroupDto.builder()
                .id(group.getId())
                .name(group.getName())
                .year(group.getYear())
                .facultyId(group.getFaculty().getId())
                .build();
    }

    public static Group toEntity(GroupDto dto){
        return Group.builder()
                .id(dto.getId())
                .name(dto.getName())
                .year(dto.getYear())
                .build();
    }
}
