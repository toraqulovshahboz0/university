package com.example.university.service.journal;

import com.example.university.dto.JournalDto;

import java.util.List;

public interface JournalService {
    JournalDto add(JournalDto dto);
    JournalDto getOne(Long id);
    JournalDto update(Long id,JournalDto dto);
    List<JournalDto> findAll();
    void delete(Long id);
}
