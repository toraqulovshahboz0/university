package com.example.university.service.journal;

import com.example.university.dto.GroupDto;
import com.example.university.dto.JournalDto;
import com.example.university.entity.Faculty;
import com.example.university.entity.Group;
import com.example.university.entity.Journal;
import com.example.university.repository.GroupRepository;
import com.example.university.repository.JournalRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.ObjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class JournalServiceImpl implements JournalService{
    private final JournalRepository repository;
    private final GroupRepository groupRepository;
    @Override
    public JournalDto add(JournalDto dto) {
        if (repository.existsByName(dto.getName()))
            throw new RuntimeException("This group already added!");
        Journal journal = JournalDto.toEntity(dto);
        Optional<Group> byId = groupRepository.findById(dto.getGroupId());
        journal.setGroup(byId.get());
        return JournalDto.toDto(repository.save(journal));
    }

    @Override
    public JournalDto getOne(Long id) {
        return JournalDto.toDto(get(id));
    }

    @Override
    public JournalDto update(Long id, JournalDto dto) {
        get(id);
        Journal newJournal=JournalDto.toEntity(dto);
        Optional<Group> byId = groupRepository.findById(dto.getGroupId());
        newJournal.setGroup(byId.get());
        return JournalDto.toDto(repository.save(newJournal));
    }

    @Override
    public List<JournalDto> findAll() {
        return repository.findAllByDeletedFalse().stream().map(JournalDto::toDto).collect(Collectors.toList());
    }

    @Override
    public void delete(Long id) {
        Journal journal=get(id);
        journal.setDeleted(true);
        repository.save(journal);

    }

    private Journal get(Long id) {
        Optional<Journal> optionalJournal = repository.findByDeletedFalseAndId(id);
        if (optionalJournal.isPresent()) {
            return optionalJournal.get();
        } else {
            throw new ObjectNotFoundException("Journal", id.toString());
        }
    }
}
