package com.example.university.service.mark;

import com.example.university.dto.MarkDto;

import java.util.List;

public interface MarkService {
    MarkDto add(MarkDto dto);
    MarkDto getOne(Long id);
    MarkDto update(Long id,MarkDto dto);
    List<MarkDto> findAll();
    void delete(Long id);
}
