package com.example.university.service.mark;

import com.example.university.dto.MarkDto;
import com.example.university.dto.SubjectDto;
import com.example.university.entity.*;
import com.example.university.repository.JournalRepository;
import com.example.university.repository.MarkRepository;
import com.example.university.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.ObjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class MarkServiceImpl implements MarkService {
    private final MarkRepository repository;
    private final StudentRepository studentRepository;
    private final JournalRepository journalRepository;

    @Override
    public MarkDto add(MarkDto dto) {
        if (repository.existsById(dto.getId()))
            throw new RuntimeException();
        Mark mark = MarkDto.toEntity(dto);
        Optional<Student> byId = studentRepository.findById(dto.getStudentId());
        mark.setStudent(byId.get());
        Optional<Journal> byId1 = journalRepository.findById(dto.getJournalId());
        mark.setJournal(byId1.get());
        return MarkDto.toDto(repository.save(mark));
    }

    @Override
    public MarkDto getOne(Long id) {
        return MarkDto.toDto(get(id));
    }

    @Override
    public MarkDto update(Long id, MarkDto dto) {
        get(id);
        Mark newMark=MarkDto.toEntity(dto);
        Optional<Student> byId = studentRepository.findById(dto.getStudentId());
        newMark.setStudent(byId.get());
        Optional<Journal> byId1 = journalRepository.findById(dto.getJournalId());
        newMark.setJournal(byId1.get());
        return MarkDto.toDto(repository.save(newMark));
    }

    @Override
    public List<MarkDto> findAll() {
        return repository.findAllByDeletedFalse().stream().map(MarkDto::toDto).collect(Collectors.toList());
    }

    @Override
    public void delete(Long id) {
        Mark mark=get(id);
        mark.setDeleted(true);
        repository.save(mark);

    }

    private Mark get(Long id) {
        Optional<Mark> optionalMark = repository.findByDeletedFalseAndId(id);
        if (optionalMark.isPresent()) {
            return optionalMark.get();
        } else {
            throw new ObjectNotFoundException("Mark", id.toString());
        }
    }
}
