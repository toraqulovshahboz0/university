package com.example.university.service.faculty;

import com.example.university.dto.FacultyDto;
import com.example.university.dto.UniversityDto;
import com.example.university.entity.Faculty;
import com.example.university.entity.University;
import com.example.university.repository.FacultyRepository;
import com.example.university.repository.UniversityRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FacultyServiceImpl implements FacultyService {
    private final UniversityRepository universityRepository;
    private final FacultyRepository repository;

    public FacultyServiceImpl(UniversityRepository universityRepository, FacultyRepository repository) {
        this.universityRepository = universityRepository;
        this.repository = repository;
    }

    @Override
    public FacultyDto add(FacultyDto dto) {
        if (repository.existsByName(dto.getName()))
            throw new RuntimeException("This faculty already added!");
        Faculty faculty = FacultyDto.toEntity(dto);
        Optional<University> findById = universityRepository.findById(dto.getUniversityId());
        faculty.setUniversity(findById.get());
        return FacultyDto.toDto(repository.save(faculty));
    }

    @Override
    public FacultyDto update(Long id, FacultyDto dto) {
        get(id);
        Faculty newFaculty = FacultyDto.toEntity(dto);
        Optional<University> findById = universityRepository.findById(dto.getUniversityId());
        newFaculty.setUniversity(findById.get());
        return FacultyDto.toDto(repository.save(newFaculty));
    }

    @Override
    public FacultyDto getOne(Long id) {
        return FacultyDto.toDto(get(id));
    }

    @Override
    public void delete(Long id) {
        Faculty faculty = get(id);
        faculty.setDeleted(true);
        repository.save(faculty);
    }

    @Override
    public List<FacultyDto> findAll() {
        return repository.findAllByDeletedFalse().stream().map(FacultyDto::toDto).collect(Collectors.toList());
    }

    private Faculty get(Long id) {
        Optional<Faculty> optionalFaculty = repository.findByDeletedFalseAndId(id);
        if (optionalFaculty.isPresent()) {
            return optionalFaculty.get();
        } else {
            throw new ObjectNotFoundException("Faculty", id.toString());
        }
    }
}
