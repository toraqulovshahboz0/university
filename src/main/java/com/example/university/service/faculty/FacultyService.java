package com.example.university.service.faculty;

import com.example.university.dto.FacultyDto;

import java.util.List;

public interface FacultyService {
    FacultyDto add(FacultyDto dto);

    FacultyDto update(Long id, FacultyDto dto);

    FacultyDto getOne(Long id);

    void delete(Long id);

    List<FacultyDto> findAll();
}
