package com.example.university.service.group;

import com.example.university.dto.GroupDto;

import java.util.List;

public interface GroupService {
    GroupDto add(GroupDto dto);
    GroupDto getOne(Long id);
    List<GroupDto> findAll();
    GroupDto update(Long id,GroupDto dto);
    void delete(Long id);
}
