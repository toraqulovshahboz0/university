package com.example.university.service.group;

import com.example.university.dto.GroupDto;
import com.example.university.entity.Faculty;
import com.example.university.entity.Group;
import com.example.university.repository.FacultyRepository;
import com.example.university.repository.GroupRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.ObjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {
    private final GroupRepository repository;
    private final FacultyRepository facultyRepository;

    @Override
    public GroupDto add(GroupDto dto) {
        if (repository.existsByName(dto.getName()))
            throw new RuntimeException("This group already added!");
        Group group=GroupDto.toEntity(dto);
        Optional<Faculty> byId = facultyRepository.findById(dto.getFacultyId());
        group.setFaculty(byId.get());
        return GroupDto.toDto(repository.save(group));
    }

    @Override
    public GroupDto getOne(Long id) {
        return GroupDto.toDto(get(id));

    }

    @Override
    public List<GroupDto> findAll() {
        return repository.findAllByDeletedFalse().stream().map(GroupDto::toDto).collect(Collectors.toList());
    }

    @Override
    public GroupDto update(Long id, GroupDto dto) {
        get(id);
        Group newGroup=GroupDto.toEntity(dto);
        Optional<Faculty> byId = facultyRepository.findById(dto.getFacultyId());
        newGroup.setFaculty(byId.get());
        return GroupDto.toDto(repository.save(newGroup));
    }

    @Override
    public void delete(Long id) {
        Group group=get(id);
        group.setDeleted(true);
        repository.save(group);

    }

    private Group get(Long id) {
        Optional<Group> optionalGroup = repository.findByDeletedFalseAndId(id);
        if (optionalGroup.isPresent()) {
            return optionalGroup.get();
        } else {
            throw new ObjectNotFoundException("Group", id.toString());
        }
    }
}
