package com.example.university.service.student;

import com.example.university.dto.StudentDto;
import com.example.university.entity.Group;
import com.example.university.entity.Student;
import com.example.university.entity.Subject;
import com.example.university.repository.GroupRepository;
import com.example.university.repository.StudentRepository;
import com.example.university.repository.SubjectRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.ObjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class StudentServiceImpl implements StudentService {
    private final StudentRepository repository;
    private final GroupRepository groupRepository;
    private final SubjectRepository subjectRepository;

    @Override
    public StudentDto add(StudentDto dto) {
        if (repository.existsByName(dto.getName()))
            throw new RuntimeException("This student already added!");
        Student student = StudentDto.toEntity(dto);
        Optional<Group> byId = groupRepository.findById(dto.getGroupId());
        student.setGroup(byId.get());
        return StudentDto.toDto(repository.save(student));

    }

    @Override
    public StudentDto getOne(Long id) {
        return StudentDto.toDto(get(id));
    }

    @Override
    public List<StudentDto> findAll() {
        return repository.findAllByDeletedFalse().stream().map(StudentDto::toDto).collect(Collectors.toList());
    }

    @Override
    public StudentDto update(Long id, StudentDto dto) {
        get(id);
        Student newStudent = StudentDto.toEntity(dto);
        Optional<Group> byId = groupRepository.findById(dto.getGroupId());
        newStudent.setGroup(byId.get());
        return StudentDto.toDto(repository.save(newStudent));
    }

    @Override
    public void delete(Long id) {
        Student student = get(id);
        student.setDeleted(true);
        repository.save(student);

    }

    @Override
    public List<StudentDto> findSubject(Long id) {
        return null;}

    private Student get(Long id) {
        Optional<Student> optionalStudent = repository.findByDeletedFalseAndId(id);
        if (optionalStudent.isPresent()) {
            return optionalStudent.get();
        } else {
            throw new ObjectNotFoundException("Student", id.toString());
        }
    }
}
