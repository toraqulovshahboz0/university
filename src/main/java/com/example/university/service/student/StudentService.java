package com.example.university.service.student;

import com.example.university.dto.StudentDto;

import java.util.List;

public interface StudentService {
    StudentDto add(StudentDto dto);
    StudentDto getOne(Long id);
    List<StudentDto> findAll();
    StudentDto update(Long id,StudentDto dto);
    void delete(Long id);
    List<StudentDto> findSubject(Long id);
}
