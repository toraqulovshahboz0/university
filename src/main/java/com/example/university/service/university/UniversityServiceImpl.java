package com.example.university.service.university;

import com.example.university.dto.UniversityDto;
import com.example.university.entity.University;
import com.example.university.repository.UniversityRepository;
import com.example.university.service.university.UniversityService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
@Service

public class UniversityServiceImpl implements UniversityService {
    private final UniversityRepository repository;

    public UniversityServiceImpl(UniversityRepository repository) {
        this.repository = repository;
    }

    @Override
    public UniversityDto add(UniversityDto dto) {
        if (repository.existsByName(dto.getName()))
            throw new RuntimeException("This university already added!");
        University university = UniversityDto.toEntity(dto);
        return UniversityDto.toDto(repository.save(university));
    }

    @Override
    public UniversityDto update(Long id, UniversityDto dto) {
        get(id);
        University newUniversity = UniversityDto.toEntity(dto);
        newUniversity.setId(id);
        repository.save(newUniversity);
        return UniversityDto.toDto(newUniversity);
    }

    @Override
    public void delete(Long id) {
        University university = get(id);
        university.setDeleted(true);
        repository.save(university);
    }

    @Override
    public UniversityDto getOne(Long id) {
        return UniversityDto.toDto(get(id));
    }

    @Override
    public List<UniversityDto> findAll() {
        return repository.findAllByDeletedFalse().stream().map(UniversityDto::toDto).collect(Collectors.toList());
    }

    private University get(Long id) {
        Optional<University> optionalUniversity = repository.findByDeletedFalseAndId(id);
        if (optionalUniversity.isPresent()) {
            return optionalUniversity.get();
        } else {
            throw new ObjectNotFoundException("University", id.toString());
        }
    }
}
