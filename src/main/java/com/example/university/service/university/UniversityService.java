package com.example.university.service.university;

import com.example.university.dto.UniversityDto;

import java.util.List;

public interface UniversityService {
    UniversityDto add(UniversityDto dto);
    UniversityDto update(Long id, UniversityDto dto);
    void delete(Long id);
    UniversityDto getOne(Long id);
    List<UniversityDto> findAll();
}
