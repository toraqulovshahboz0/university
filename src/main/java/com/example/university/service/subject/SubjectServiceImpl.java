package com.example.university.service.subject;

import com.example.university.dto.StudentDto;
import com.example.university.dto.SubjectDto;
import com.example.university.entity.Group;
import com.example.university.entity.Student;
import com.example.university.entity.Subject;
import com.example.university.repository.GroupRepository;
import com.example.university.repository.SubjectRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.ObjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class SubjectServiceImpl implements SubjectService{
    private final SubjectRepository repository;
    private final GroupRepository groupRepository;
    @Override
    public SubjectDto add(SubjectDto dto) {
        if (repository.existsByName(dto.getName()))
            throw new RuntimeException("This subject already added!");
        if (repository.existsById(dto.getId()))
            throw new RuntimeException();
        Subject subject = SubjectDto.toEntity(dto);
        Optional<Group> byId = groupRepository.findById(dto.getGroupId());
        subject.setGroup(byId.get());
        return SubjectDto.toDto(repository.save(subject));
    }

    @Override
    public SubjectDto getOne(Long id) {
        return SubjectDto.toDto(get(id));
    }

    @Override
    public List<SubjectDto> findAll() {
        return repository.findAllByDeletedFalse().stream().map(SubjectDto::toDto).collect(Collectors.toList());
    }

    @Override
    public SubjectDto update(Long id, SubjectDto dto) {
        get(id);
        Subject newSubject=SubjectDto.toEntity(dto);
        Optional<Group> byId = groupRepository.findById(dto.getGroupId());
        newSubject.setGroup(byId.get());
        return SubjectDto.toDto(repository.save(newSubject));
    }

    @Override
    public void delete(Long id) {
        Subject subject=get(id);
        subject.setDeleted(true);
        repository.save(subject);

    }
    private Subject get(Long id) {
        Optional<Subject> optionalSubject = repository.findByDeletedFalseAndId(id);
        if (optionalSubject.isPresent()) {
            return optionalSubject.get();
        } else {
            throw new ObjectNotFoundException("Student", id.toString());
        }
    }
}
