package com.example.university.service.subject;

import com.example.university.dto.SubjectDto;

import java.util.List;

public interface SubjectService {
    SubjectDto add(SubjectDto dto);

    SubjectDto getOne(Long id);

    List<SubjectDto> findAll();

    SubjectDto update(Long id, SubjectDto dto);

    void delete(Long id);
}
